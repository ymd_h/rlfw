import gym

import tensorflow as tf

from rlfw.parameter import LinearAnnealingParameter
from rlfw.discrete.value import QFunction
from rlfw.discrete.policy import RandomPolicy, GreedyPolicy, EpsilonGreedyPolicy
from rlfw.discrete.algorithm import DQN
from rlfw.util import clone_model, evaluate, prepare_log

env_name = "CartPole-v1"
env = gym.make(env_name)
eval_env = gym.make(env_name)


buffer_size = int(1e+6)
batch_size = 512

n_warmup = 100
n_training = int(1e+5)
eval_freq = 500

prepare_log()

# Value Function
Q = QFunction(state_shape=env.observation_space.shape,
              act_size=env.action_space.n,
              fully_connected=(64, 64, 64))


# Policy
randomP = RandomPolicy(act_size=env.action_space.n)
egreedyP = EpsilonGreedyPolicy(act_size=env.action_space.n,
                               Q=Q,
                               eps=LinearAnnealingParameter("e-greedy",
                                                            0.1,-1e-5,0.01))
greedyP = GreedyPolicy(Q=Q)

# Algorithm
dqn = DQN(Q=Q, gamma=0.995, Nstep=3, prioritized=True,
          target_update_freq=5000, double_DQN=True,
          optimizer=tf.optimizers.Adam(learning_rate=1e-4))

# Replay Buffer
rb = dqn.create_buffer(buffer_size, env)
beta = LinearAnnealingParameter("PER-beta",0.4, 0.00005, 1.0)

# Warmup
obs = env.reset()
for _ in range(n_warmup):
    act = randomP(obs)
    next_obs, rew, done, _ = env.step(act)
    rb.add(obs=obs, act=act, rew=rew, next_obs=next_obs, done=done)

    if done:
        rb.on_episode_end()
        obs = env.reset()
    else:
        obs = next_obs


for step in range(n_training):
    act = egreedyP(obs)
    next_obs, rew, done, _ = env.step(act)
    rb.add(obs=obs, act=act, rew=rew, next_obs=next_obs, done=done)


    if done:
        rb.on_episode_end()
        obs = env.reset()
    else:
        obs = next_obs


    sample = rb.sample(batch_size, beta.get_value(step))
    absTD = dqn.train(sample)

    if dqn.prioritized:
        rb.update_priorities(sample["indexes"], absTD)

    if step % eval_freq == 0:
        evaluate(eval_env, greedyP, step, ntimes=10)
