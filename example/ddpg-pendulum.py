import gym

import tensorflow as tf

from rlfw.parameter import LinearAnnealingParameter
from rlfw.continuous.value import QFunction
from rlfw.continuous.policy import GreedyPolicy, GaussianNoisePolicy
from rlfw.continuous.algorithm import DDPG
from rlfw.util import clone_model, evaluate, prepare_log

env_name = "Pendulum-v1"
env = gym.make(env_name)
eval_env = gym.make(env_name)

buffer_size = int(1e+6)
batch_size = 512

n_warmup = 100
n_training = int(1.2e+4)
eval_freq = 500

prepare_log()


# Value Function
Q = QFunction(state_shape=env.observation_space.shape,
              act_dim=env.action_space.shape,
              fully_connected=(100,100,100))


# Policy
greedyP = GreedyPolicy(state_shape=env.observation_space.shape,
                       act_dim=env.action_space.shape,
                       fully_connected=(100,100,100),
                       act_min=env.action_space.low,
                       act_max=env.action_space.high)
gaussP = GaussianNoisePolicy(policy=greedyP,
                             noise_sigma=LinearAnnealingParameter("GaussianNoisePolicy",
                                                                  0.02, -1e-7, 1e-3),
                             act_min=env.action_space.low,
                             act_max=env.action_space.high)


# Algorithm
ddpg = DDPG(Q=Q, pi=greedyP, polyak=0.995, gamma=0.99, Nstep=3, prioritized=True)


# Replay Buffer
rb = ddpg.create_buffer(buffer_size, env)
beta = LinearAnnealingParameter("PER-beta", 0.4, 0.00001, 1.0)


# Warmup
obs = env.reset()
for _ in range(n_warmup):
    act = gaussP(obs)
    next_obs, rew, done, _ = env.step(act)
    rb.add(obs=obs, act=act, rew=rew, next_obs=next_obs, done=done)

    if done:
        rb.on_episode_end()
        obs = env.reset()
    else:
        obs = next_obs


for step in range(n_training):
    act = gaussP(obs)
    next_obs, rew, done, _ = env.step(act)
    rb.add(obs=obs, act=act, rew=rew, next_obs=next_obs, done=done)

    if done:
        rb.on_episode_end()
        obs = env.reset()
    else:
        obs = next_obs

    sample = rb.sample(batch_size, beta.get_value(step))
    absTD = ddpg.train(sample)

    if ddpg.prioritized:
        rb.update_priorities(sample["indexes"], absTD)

    if step % eval_freq == 0:
        evaluate(eval_env, greedyP, step, ntimes=10)
