* Change Log

** Unreleased
- Fix: Tensoarboard plugin active/inactive check
- Add: Reward Aggregation plugin
- Add: Gradient Clipping at DQN

** [[https://gitlab.com/ymd_h/rlfw/-/tags/v0.0.0][v0.0.0]]
- Add: DQN with CartPole-v1 example
- Add: DDPG with Pendulum-v0 example

