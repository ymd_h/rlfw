import tensorflow as tf

from rlfw.parameter import HyperParameter, ConstantParameter
from rlfw.util import random_generator, create_counter


class Policy(tf.keras.Model):
    def __init__(self, act_min, act_max, *,
                 dtype = tf.float32):
        super().__init__(dtype=dtype)

        self.has_minmax = False
        self.act_min = None
        if act_min is not None:
            self.act_min = tf.constant(act_min, dtype=dtype).numpy()
            self.has_minmax = True

        self.act_max = None
        if act_max is not None:
            self.act_max = tf.constant(act_max, dtype=dtype).numpy()
            self.has_minmax = True

    def call(self, state, *args, **kwargs):
        """
        Get an action from a state

        Parameters
        ----------
        state : array-like
            Single state

        Reeturns
        --------
        action : np.ndarray
            An action obeying policy
        """
        state = tf.expand_dims(tf.constant(state, dtype=self.dtype), axis=0)
        v = tf.squeeze(self.get_action(state), axis=0).numpy()

        if self.has_minmax:
            v = v.clip(min=self.act_min, max=self.act_max)

        return v

    def get_action(self, state: tf.Tensor):
        raise NotImplementedError

    def get_config(self):
        return {"act_min": self.act_min, "act_max": self.act_max, "dtype": self.dtype}


class GreedyPolicy(Policy):
    """
    Greedy Policy
    """
    def __init__(self, state_shape, act_dim, act_min, act_max,
                 fully_connected = (100, 100, 100), *, dtype = tf.float32):
        """
        Initialize Greedy Policy

        Parameters
        ----------
        state_shape : array-like of int
            Shape of state
        act_dim : int
            Dimension of action
        act_min : float
            Minimum action
        act_max : float
            Maximum action
        fully_connected : array_like of int, option
            Numbers of units at fully connected layers.
            Default is `(100, 100, 100)`
        """
        super().__init__(act_min=act_min, act_max=act_max, dtype=dtype)

        act_size = 1
        if isinstance(act_dim, int):
            act_size = act_dim
        else:
            for i in act_dim:
                act_size *= i

        FC = tf.keras.layers.Dense
        self.i = tf.keras.layers.InputLayer(input_shape=state_shape, dtype=dtype)
        self.fc = [FC(u, activation="relu", dtype=dtype) for u in fully_connected]
        self.o = FC(act_size, activation="tanh", dtype=dtype)

        self.center = tf.constant((act_min + act_max)/2, dtype=dtype)
        self.scale  = tf.constant((act_max - act_min)/2, dtype=dtype)

        self.get_action(tf.expand_dims(tf.zeros(shape=state_shape, dtype=dtype),
                                       axis=0))

    @tf.function
    def get_action(self, state: tf.Tensor):
        state = self.i(state)

        for L in self.fc:
            state = L(state)

        return self.o(state) * self.scale + self.center

    def get_weights(self):
        weights = []

        weights.append([L.get_weights() for L in self.fc])
        weights.append(self.o.get_weights())

        return weights

    def set_weights(self, weights):
        fc, output = weights

        for L, w in zip(self.fc, fc):
            L.set_weights(w)

        self.o.set_weights(output)

    def get_config(self):
        return {**super().get_config(),
                "state_shape": self.i.get_config()["batch_input_shape"][1:],
                "act_dim": self.o.units,
                "fully_connected": (L.units for L in self.fc)}


class GaussianNoisePolicy(Policy):
    """
    Gaussian Noise Policy
    """
    def __init__(self, policy: Policy,
                 noise_sigma: HyperParameter,
                 act_min, act_max):
        """
        Initialize GaussianNoisePolicy

        Parameters
        ----------
        policy : Policy
            Policy to add noise to
        noise_sigma : HyperParameter or float
            Gaussian Noise standard deviation.
        act_min : float
            Minimum action
        act_max : float
            Maximum action
        """
        super().__init__(act_min=act_min, act_max=act_max, dtype=policy.dtype)

        self.policy = policy

        if isinstance(noise_sigma, HyperParameter):
            self.noise_sigma = noise_sigma
        else:
            self.noise_sigma = ConstantParameter("GaussianNoisePolicy/sigma",
                                                 noise_sigma, dtype=self.dtype)

        self.rng = random_generator()
        self.counts = create_counter()

    @tf.function
    def get_action(self, state: tf.Tensor):
        action = self.policy.get_action(state)

        noise = self.rng.normal(action.shape,
                                stddev=self.noise_sigma.get_value(self.counts))
        self.counts.assign_add(1)
        return action + noise

    def get_weights(self):
        return self.policy.get_weights()

    def set_weights(self, weights):
        self.policy.set_weights(weights)

    def get_config(self):
        return {**super().get_config(),
                "policy": self.policy,
                "noise_sigma": self.noise_sigma}
