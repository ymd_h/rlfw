export async function render(){
    const style = document.createElement("style");
    style.innerText = `
.graph-wrapper {
  display: inline-block;
}`;
    document.head.appendChild(style);

    const wrapper = document.createElement("div");
    wrapper.className = "wrapper"

    const form = document.createElement("form");
    form.id = "runs";

    const button = document.createElement("button");
    button.button = "button";
    button.textContent = "Aggregate Runs";
    button.onclick = aggregate;
    form.appendChild(button);

    wrapper.appendChild(form);
    reload();

    const graph = document.createElement("div");
    graph.id = "graph";

    const dist = document.createElement("div");
    dist.id = "dist";
    dist.className = "graph-wrapper";
    graph.appendChild(dist);

    wrapper.appendChild(graph);
    document.body.appendChild(wrapper);
}

async function reload(){
    const runToTags = await fetch("./tags").then((response) => response.json());

    const form = document.getElementById("runs");
    for (const [key, value] of Object.entries(runToTags)) {
	if(document.getElementById(key)){ continue; }

	const div = document.createElement("div");

	const input = document.createElement("input");
	input.type = "checkbox";
	input.id = key;
	input.name = "runs";
	input.value = key;
	div.appendChild(input);

	const label = document.createElement("label");
	label.for = key;
	label.textContent = key
	div.appendChild(label);

	form.appendChild(div);
    }

}


function aggregate(){
    const form = new FormData(document.getElementById("runs"));
    const param = new URLSearchParams(form);

    fetch("./agg?" + param)
	.then(response => {
	    if(response.ok){ return response.json(); }
	    throw new Error();
	})
	.then(data => {
	    let dist_img = document.getElementById("dist_img");
	    if(dist_img === null){
		dist_img = document.createElement("img");
		dist_img.id = "dist_img";
		document.getElementById("dist").appendChild(dist_img);
	    }
	    dist_img.src = "data:image/png:base64," + data["distribution"];
	}).catch((reason)=>{ console.log(reason); });

    return false;
}

(function(){
    var last = null;

    setInterval(()=>{
	const rbtn = window.parent.document.getElementsByClassName('reload-button')[0];

	if(last != rbtn.title){
	    reload();
	    last = rbtn.title;
	}
    }, 1000);
})();
