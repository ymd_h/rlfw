import json
import os

import numpy as np
import matplotlib.pyplot as plt
from io import BytesIO
import urllib
from matplotlib.backends.backend_agg import FigureCanvasAgg
import werkzeug

import tensorflow as tf
from tensorboard import plugin_util
from tensorboard.plugins.base_plugin import TBPlugin, FrontendMetadata
from tensorboard.plugins.scalar import metadata as scalars_md
from tensorboard.plugins.scalar import scalars_plugin


def fig2png(fig):
    canvas = FigureCanvasAgg(fig)
    png = BytesIO()
    canvas.print_png(png)
    img = urllib.parse.quote(png.getvalue())
    return img

def nonsniff_response(data, content_type):
    response = werkzeug.Response(data, content_type=content_type)
    response.headers['X-Content-Type-Options'] = 'nosniff'
    return response

class RewardAggregationPlugin(TBPlugin):
    plugin_name = "reward_aggregation_plugin"

    def __init__(self, context):
        self.multiplexer = context.multiplexer
        self.last_data = None

        tag_base = "Evaluation/Reward/"
        self.m_tag = tag_base + "mean"
        self.v_tag = tag_base + "variance"

    def get_plugin_apps(self):
        return {"/index.js": self._serve_js,
                "/tags": self._serve_tags,
                "/agg": self._serve_aggregation}

    def is_active(self):
        for run in self.multiplexer.Runs():
            acc = self.multiplexer.GetAccumulator(run)
            try:
                acc.Tensors(self.m_tag)
                return True
            except KeyError:
                pass

        return False

    def frontend_metadata(self):
        return FrontendMetadata(es_module_path = "/index.js",
                                tab_name = "Reward Aggregation")

    @werkzeug.wrappers.Request.application
    def _serve_js(self, request):
        del request
        file = os.path.join(os.path.dirname(__file__), "static", "index.js")

        with open(file) as f:
            js = f.read()

        return nonsniff_response(js, "application/javascript")


    @werkzeug.wrappers.Request.application
    def _serve_tags(self, request):
        del request

        mapping = self.multiplexer.PluginRunToTagToContent(scalars_md.PLUGIN_NAME)
        result = {run: {} for run in self.multiplexer.Runs()}

        for run, tag_to_content in mapping.items():
            for tag in tag_to_content:
                metadata = self.multiplexer.SummaryMetadata(run, tag)
                result[run][tag] = {"description": metadata.summary_description}
        tags = json.dumps(result, sort_keys=True)

        return nonsniff_response(tags, "application/json")


    @werkzeug.wrappers.Request.application
    def _serve_aggregation(self, request):
        runs = request.args.getlist("runs")

        accs = [(r, self.multiplexer.GetAccumulator(r)) for r in runs]

        m_agg = self._aggregate(accs, self.m_tag)
        v_agg = self._aggregate(accs, self.v_tag)

        img, x, m, v = self._dist_plot(m_agg, v_agg)

        data = {"distribution": img}
        data = json.dumps(data, sort_keys=True)

        self.last_data = {"step": x, "mean": m, "variance": v}
        return nonsniff_response(data, "application/json")

    def _aggregate(self, accs, tag):
        agg = {}

        for r, a in accs:
            try:
                center = a.Tensors(tag)
                upper = a.Tensors(tag + "_upper")
                lower = a.Tensors(tag + "_lower")
            except KeyError:
                continue

            for c, u, l in zip(center, upper, lower):
                step = c.step

                if (step != u.step) or (step != l.step):
                    continue

                if step not in agg:
                    agg[step] = {"mean": [], "variance": []}

                cv = tf.make_ndarray(c.tensor_proto)
                uv = tf.make_ndarray(u.tensor_proto)
                lv = tf.make_ndarray(l.tensor_proto)

                agg[step]["mean"].append(cv)
                agg[step]["variance"].append(np.square((uv - lv)*0.5))

        return agg

    def _dist_plot(self, m_agg, v_agg):
        x = []
        m = []
        v = []

        for step in m_agg:
            if step not in v_agg:
                continue

            # cf. https://stats.stackexchange.com/questions/43159/how-to-calculate-pooled-variance-of-two-or-more-groups-given-known-group-varianc
            mean = np.asarray(m_agg[step]["mean"])
            variance = np.asarray(v_agg[step]["mean"])
            N = mean.shape[0]

            if N != variance.shape[0]:
                continue

            x.append(step)
            m.append(mean.mean())
            v.append((np.square(mean).sum() + variance.sum() - N*np.square(m[-1]))/(N-1))


        x = np.asarray(x)
        m = np.asarray(m)
        v = np.asarray(v)

        fig = plt.figure(figsize=(7, 5))
        plt.plot(x, m, color="tab:blue")
        std = np.sqrt(v)
        plt.fill_between(x, m+std, m-std, color="tab:blue", alpha=0.4)
        plt.title("Estimated Reward Distribution")

        img = fig2png(fig)
        return img, x, m, v
