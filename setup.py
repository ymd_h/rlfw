from setuptools import setup, find_packages


setup(name="rlfw",
      version="0.0.0",
      author="Yamada, Hiroyuki",
      install_requires = ["tensorflow>=2", "cpprb", "werkzeug", "matplotlib"],
      description = "Reinforcement Learning Framework",
      packages = find_packages("."),
      classifiers=["Programming Language :: Python",
                   "Programming Language :: Python :: 3",
                   "License :: OSI Approved :: MIT License",
                   "Operating System :: OS Independent",
                   "Development Status :: 2 - Pre-Alpha",
                   "Intended Audience :: Developers",
                   "Intended Audience :: Science/Research",
                   "Topic :: Scientific/Engineering",
                   "Topic :: Scientific/Engineering :: Artificial Intelligence",
                   "Topic :: Software Development :: Libraries"],
      package_data={"rlfw": ["static/**"]},
      entry_points={"tensorboard_plugins":
                    ["RewardAggregation = rlfw.plugin:RewardAggregationPlugin"]})
